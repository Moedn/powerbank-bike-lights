---
title: Assembly Instructions
...

# Preperation

## Mounts

- adjust parameters in openSCAD files so that the mounts fit for your bike
	+ `D_rod` is the diameter (in `mm`) of the rod on which the mount is supposed to be zipped to
		* note that you should also adjust the added rubber thickness there in case you're using thicker material
	+ `angle_rod` is the angle (in `deg`) of the rod normal to the ground (so `0` would equal a vertical bar, `90` a horizontal bar; your value will be typically around `20`)
	+ adjust `t_cable` if you must, e.g. when you're using thicker cables
- export new STL files
- print mounts
	+ find printing parameters in the manifest file ([okh-powerbank-bike-lights.toml](okh-powerbank-bike-lights.toml))

## xxx

- cut bicycle tube (or any other rubber material) so it fits into the mount

# Assembly

## Soldering the Circuit

- basic circuit: USB connector VCC → parallel(R_white → LED_white ; R_red → LED_red) → USB connector GND
- solder USB connector
	+ leftmost is GND, rightmost is VCC (5V) (see picture below)\
	![USB Connector Pinouts](https://www.hobbytronics.co.uk/image/data/tutorial/usb-connectors.jpg){ width=50% }

## 