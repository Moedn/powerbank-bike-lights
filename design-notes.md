# Design Notes

## Mounts

### Formulas

> mainly for backup reasons; I was too lazy to provide a sketch and derivation for them

distance between LED plane and the bottom of the design, so that the seatpost never penetrates the LED plane (3mm distance):

`D_seatpost / cos(ang_seatpost) * 1 / 2 + D_led / 2 * tan(ang_seatpost) + 3mm`

angle of wire tube plane, so that the wire come out in half of the angle between horizontal and the seatpost:

`45deg-ang_seatpost/2`

offset for wire tube pad so that extrusion doesn't start inside the LED pocket (tolerance for LED width =1mm; wall thickness of wire tube =2mm):

`(D_led+1mm)/2*cos(ang_seatpost)+D_wireshell/(2*tan(ang_seatpost))`

### Moe's Parameters

#### Rear

| ID           | Value [mm, deg]                                | Detail                                                         |
|--------------|------------------------------------------------|----------------------------------------------------------------|
| D_seatpost   | 27                                             | diameter of seatpost (gets added +1mm rubber insert thickness) |
| ang_seatpost | 18                                             | angle of seatpost normal to ground                             |
| D_glass      | 30                                             | diameter of glass                                              |
| t_glass      | 3                                              | thickness of glass                                             |
| a_glue       | 1.5                                            | gluing width                                                   |
| D_led        | 21                                             | diameter of LED                                                |
| h_led        | 2.1                                            | height of LED                                                  |
| h_light      | 10                                             | optical distance from LED to glass to get scattered light      |
| h_claw       | 10                                             | height of claw                                                 |
| d_o          | 29.82                                          | inner diameter of O-ring (ISO-3601-123)                        |
| t_o          | 2.62                                           | thickness of O-ring                                            |
| D_wireshell  | 5                                              | outer diameter of bowden cable shell                           |
| l_o          | =B11 * pi                                      | lenght of O-ring string                                        |
| l_1          | =((B2 / 2 + B12 / 2) * pi + B10 + 2 * B12) * 2 | path lenght for O-ring                                         |
| stretch      | =E12 / E11                                     | resulting O-ring stretching (should be 1,3…1,7)                |

#### Front

| ID           | Value [mm, deg] | Detail                                                         |
|--------------|-----------------|----------------------------------------------------------------|
| D_seatpost   | 39              | diameter of seatpost (gets added +1mm rubber insert thickness) |
| ang_seatpost | 20              | angle of seatpost normal to ground                             |
| D_glass      | 30              | diameter of glass                                              |
| t_glass      | 3               | thickness of glass                                             |
| a_glue       | 1.5             | gluing width                                                   |
| D_led        | 21              | diameter of LED                                                |
| h_led        | 2.1             | height of LED                                                  |
| h_light      | 10              | optical distance from LED to glass to get scattered light      |
| d_o          |                 | inner diameter of O-ring (ISO-3601-123)                        |
| t_o          |                 | thickness of O-ring                                            |
| D_wireshell  | 5               | outer diameter of bowden cable shell                           |
| l_o          |                 | lenght of O-ring string                                        |
| l_1          |                 | path lenght for O-ring                                         |
| stretch      |                 | resulting O-ring stretching (should be 1,3…1,7)                |


---

- not meant for plug'n'play
	- zip ties for easy installation
- cable holes should ensure ventilation but prevent mud
- perspex windows can be glued
	+ ø = 30mm is hard to get; however, custom cuts are quite effortable (e.g. <http://www.acrylformen.de/shop/de/Plexi-/-Acrylglaszuschnitt/Rund-/-Kreis/Plexiglas-GS-Satiniert-Farbig/Plexiglas-Rund-SATINICE-Weiss-snow-3-mm-Stark->)

## LEDs

- 3W high power LED white, star model
	+ https://www.ebay.de/itm/223012126314
	* 10000k - 20000k white
	* 750mA
	* 3,5 V bis 4,5 V
	* 240lm -260lm (120°)
	* at a 5V supply: $ R_{white} = \frac{(5-3.5…4.5)V}{0.75A}= 0.67…2 \Omega $
		- I used 1R || 2R2
- 3W high power LED red, star model
+ https://www.ebay.de/itm/323767523967
	* red, 610nm - 630nm
	* 700mA
	* 2,2 V bis 2,8 V
	* 60lm - 70lm (120°)
	* at a 5V supply: $ R_{red} = \frac{(5-2.2…2.8)V}{0.7A}= 3.14…4 \Omega $
		- I used 1R7 || 10R

## Cables

I put both cables in one shift cable housing, however the only cables I had available that would still fit both inside that housing had a copper diameter of 0.15 mm.

At ~0.5 m cable length this results into a voltage drop of ~3.5 %.

# Inspirations

- https://www.thingiverse.com/thing:195179
- https://www.thingiverse.com/thing:3091337
- https://www.thingiverse.com/thing:2965812
- https://www.thingiverse.com/thing:405919
- https://www.thingiverse.com/thing:3651
